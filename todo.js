var TodoList = Backbone.Marionette.LayoutView.extend({
  el: '#app-hook',
  template: '#Hello'
});

var todo = new TodoList({
  model: new Backbone.Model({
    items: [
      {assignee: 'Scott', text: 'Write a book about Marionette'},
      {assignee: 'Andrew', text: 'Do some coding'}
    ]
  })
});

todo.render();


// var ToDo = Backbone.Marionette.LayoutView.extend({
//   tagName: 'li',
//   template: '#todo'
// });


// var TodoList = Backbone.Marionette.CollectionView.extend({
//   el: '#app-hook',
//   tagName: 'ul',

//   childView: ToDo
// });

// var todo = new TodoList({
//   collection: new Backbone.Collection([
//     {assignee: 'Scott', text: 'Write a book about Marionette'},
//     {assignee: 'Andrew', text: 'Do some coding'}
//   ])
// });

// todo.render();