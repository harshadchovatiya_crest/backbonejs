// var myModel = Backbone.Model.extend({
// 	defaults: {
// 		name: ""
// 	}
// });

// var myCollection = Backbone.Collection.extend({
// 	model: myModel
// });

// var m1 = new myModel({name: "ABC"});
// var m2 = new myModel({name: "DEF"});
// var m3 = new myModel({name: "GHI"});
// var m4 = new myModel({name: "JKL"});

// var collect = new myCollection();
// collect.add(m1);
// collect.add(m2);
// collect.add(m3);
// collect.add(m4);

// var myView = Marionette.ItemView.extend({
// 	tagName: 'li',
// 	template: '#what-to-display',
// 	events: {
// 		"mouseenter #info": "showDetails"
// 	},
// 	showDetails: function() {
// 		alert(this.model.get('name'))
// 	}
// });

// var myView2 = Marionette.CollectionView.extend({
// 	tagName: 'ul',
// 	el: '#display-here',
// 	childView: myView
// });

// var x = new myView2({collection:collect});
// x.render();


var model1 = Backbone.Model.extend({
	defaults:  {
		name: "ABC"
	}
});

var model2 = Backbone.Model.extend({
	defaults: {
		foo: "Foo"
	}
});

var view1 = Marionette.ItemView.extend({
	el: '#display-here',
	getTemplate: function() {
		if(this.model.get('name')){
			return '#name-template'
		}
		else {
			return '#foo-template'
		}
	}
});

var app = new Marionette.Application({
	onStart: function() {
		var x2 = new model2();
		var v1 = new view1({model: x2});
		v1.render();		
	}
});

app.on("start", function() {
	console.log("On start...")
});

app.on("before:start", function(){
	console.log("Before start...");
});

app.start();
