var Record = Backbone.Model.extend({
	defaults: {
		device: ['GT-I9300','HTC Desire','C1605','GT-S5360','GT-I9100'],
		session: [0,0,0,0,0]
	}
});

var graphview = Backbone.View.extend({
	el: $('#div1'),
	model: new Record(),
	template: _.template($('#add-data').html()),
	initialize: function(){
		this.$el.html(this.template());
	},
	events: {
		'click button': 'adddata'
	},
	adddata: function(){
		var devices = this.model.get('device');
		var t = -1;
		for (var i = devices.length - 1; i >= 0; i--) {
			if(devices[i] == $('#device').val()) {
				t = i;
				break;
			}
		}
		if(t == -1) {
			alert("Invalid device name");
		}
		else {
			var sessions = this.model.get('session');
			if (isNaN($('#session').val()) || $('#session').val()=="") {
				alert("Invalid session value");
			}
			else {
				sessions[t] = parseFloat($('#session').val());
				this.model.set({ 'device' : devices, 'session': sessions});
			}  
		}
		console.log(this.model.get('device'));
		console.log(this.model.get('session'));
		this.render();
	},
	render: function(){
		var chart = {
		   	type: 'column'
		};
		var title = {
		   	text: 'Top devices by sessions'   
		};
		var xAxis = {
			title: {
				text: 'Devices'
			},
			categories: this.model.get('device'),
		   	crosshair: true
		};
		var yAxis = {
		   	min: 0,
		   	title: {
		    	text: 'Sessions'
		   	}
		};
		var tooltip = {
		   headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		   pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		   '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
		   footerFormat: '</table>',
		   shared: true,
		   useHTML: true
		};
		var plotOptions = {
		   column: {
		      pointPadding: 0.2,
		      borderWidth: 0
		   }
		};
		var series = [
		{
		   name: 'Sessions',
		   data: this.model.get('session')
		}
		];
		var json = {};
		json.chart = chart;
		json.title = title;
		json.tooltip = tooltip;
		json.xAxis = xAxis;
		json.yAxis = yAxis;
		json.series = series;
		json.plotOptions = plotOptions;
		$('#container').highcharts(json);
		t.render();		
	}
});

var x = new graphview();

var tableview = Backbone.View.extend({
	el: $('.items-list'),
	template: _.template($('#display-data').html()),
	initialize: function(){
	},
	render: function() {
		devices = x.model.get('device');
		sessions = x.model.get('session');
		console.log("Inside table");
		this.$el.html('');
		for (var i = 0; i <= devices.length - 1; i++) {
			this.$el.append(this.template({device: devices[i], session: sessions[i]}));
		}
	}
});

var t = new tableview();

$(document).ready(function() {
	x.render()
})