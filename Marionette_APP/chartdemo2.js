var Record = Backbone.Model.extend({
	defaults: {
		device: ['GT-I9300','HTC Desire','C1605','GT-S5360','GT-I9100'],
		session: [0,0,0,0,0]
	}
});

var graphView = Marionette.CompositeView.extend({
	el: '#div1',
	model: new Record(),
	template: '#add-data',
	events: {
		'click button': 'adddata'
	},
	// modelEvents: {
 //   		change: 'itemAdded'
 //  	},
	ui: {
		device: '#device',
		session: '#session'
	},
	adddata: function(){
		var devices = this.model.get('device');
		var t = -1;
		for (var i = devices.length - 1; i >= 0; i--) {
			if(devices[i] == this.ui.device.val()) {
				t = i;
				break;
			}
		}
		if(t == -1) {
			alert("Invalid device name");
		}
		else {
			var sessions = this.model.get('session');
			if (isNaN(this.ui.session.val()) || this.ui.session.val()=="") {
				alert("Invalid session value");
			}
			else {
				sessions[t] = parseFloat(this.ui.session.val());
				this.model.set({ 'device' : devices, 'session': sessions});
			}  
		}
		console.log(this.model.get('device'));
		console.log(this.model.get('session'));
		// this.render();
		this.itemAdded();
	},
	itemAdded: function() {
		this.ui.session.val("");
		console.log("Working");
		var chart = {
		   	type: 'column'
		};
		var title = {
		   	text: 'Top devices by sessions'   
		};
		var xAxis = {
			title: {
				text: 'Devices'
			},
			categories: this.model.get('device'),
		   	crosshair: true
		};
		var yAxis = {
		   	min: 0,
		   	title: {
		    	text: 'Sessions'
		   	}
		};
		var tooltip = {
		   headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		   pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		   '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
		   footerFormat: '</table>',
		   shared: true,
		   useHTML: true
		};
		var plotOptions = {
		   column: {
		      pointPadding: 0.2,
		      borderWidth: 0
		   }
		};
		var series = [
		{
		   name: 'Sessions',
		   data: this.model.get('session')
		}
		];
		var json = {};
		json.chart = chart;
		json.title = title;
		json.tooltip = tooltip;
		json.xAxis = xAxis;
		json.yAxis = yAxis;
		json.series = series;
		json.plotOptions = plotOptions;
		$('#container').highcharts(json);
	}
});

var x = new graphView();
x.render();

var tableview = Marionette.View.extend({
	el: '.items-list',
	template: _.template($('#display-data').html()),

	render: function() {
		devices = x.model.get('device');
		sessions = x.model.get('session');
		console.log("Inside table");
		this.$el.html('');
		for (var i = 0; i <= devices.length - 1; i++) {
			this.$el.append(this.template({device: devices[i], session: sessions[i]}));
		}
	}
});

var t = new tableview();

$(document).ready(function() {
	t.render();
	x.itemAdded();
})