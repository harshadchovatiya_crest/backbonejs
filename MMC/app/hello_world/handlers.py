# -*- coding: utf-8 -*-
"""
    hello_world.handlers
    ~~~~~~~~~~~~~~~~~~~~

    Hello, World!: the simplest tipfy app.

    :copyright: 2009 by tipfy.org.
    :license: BSD, see LICENSE for more details.
"""
from tipfy.app import Response
from tipfy.handler import RequestHandler
from tipfyext.jinja2 import Jinja2Mixin
from google.appengine.ext import db


class HelloWorldHandler(RequestHandler):
    def get(self):
        """Simply returns a Response object with an enigmatic salutation."""
        return Response('Hello, World!')


class PrettyHelloWorldHandler(RequestHandler, Jinja2Mixin):
    def get(self):
        """Simply returns a rendered template with an enigmatic salutation."""
        context = {
            'message': 'Hello, World!',
        }
        return self.render_response('hello_world.html', **context)

class FirstHelloWorldHandler(RequestHandler, Jinja2Mixin):
    def get(self):
        arr = [10,15]
        """Simply returns a rendered template with an enigmatic salutation."""
        context = {
            'message': 'First page working fine!',
            'arr': arr,
        }
        return self.render_response('hello_world.html', **context)


class DeviceBySession(db.Model):
    device = db.StringProperty()
    session = db.StringProperty()

    @classmethod
    def get_by_device(cls, device_name):
        return cls.all().filter('device=',device_name)


class DemoHandler(RequestHandler, Jinja2Mixin):
    def get(self):
        devices = ["GT-I9300","HTC Desire", "C1605","GT-S5360" ,"GT-I9100", "GT-S53160" ]
        # session = [0,0,0,0,0]
        """Simply returns a rendered template with an enigmatic salutation."""
        session = [0,0,0,0,0,0]
        x = DeviceBySession.all()
        # x.filter("device =", "GT-I9300")
        # for i in x:
        #     print(i.device)
        #     i.session = "400"
        #     i.put()
        for raw in x:
            device_name = raw.device
            for i in range(6):
                if devices[i] == device_name:
                    session[i] = int(raw.session)
        

        # x = DeviceBySession.all()
        # print(type(x))
        # for z in x:
        #     z.session = "222"
        #     # print(z.key_name)
        #     z.put()
        # for device in devices:
        #     x = DeviceBySession.get_by_key_name(device)
        #     print(type(x))
        #     print(x.session)
        #     for z in x:
        #         z.session = "123"
        #         z.put()
        #         print(z)
        context = {
            'message': 'Top devices by Session!',
            'devices': devices,
            'session': session
        }
        return self.render_response('demo.html', **context)

    def post(self):
        self.session = self.request.form.get('session-value')
        self.device = self.request.form.get('device-name')
        try:
             session_val = int(self.session)
        except ValueError:
             self.session = "0"

        self.device_by_session = DeviceBySession(key_name=self.device, device=self.device, session=self.session)
        self.device_by_session.put()
        
        # session = [0,0,0,0,0,0]
        # x = DeviceBySession.all()
        # for raw in x:
        #     device_name = raw.device
        #     for i in range(6):
        #         if devices[i] == device_name:
        #             session[i] = int(raw.session)
        # context = {
        #     'devices': devices,
        #     'message': 'Top devices by Session',
        #     'session': session
        # }
        # return self.render_response('demo.html' , **context)
        return self.redirect('demo')


class UserTaskDetail(db.Model):
    assignee = db.StringProperty()
    task = db.StringProperty()

error = ""
class UserHandler(RequestHandler, Jinja2Mixin):
    def get(self):
        global error
        # assignee = "assignee"
        # text = "text"
        all_rows = UserTaskDetail.all()
        data_info = []
        for row in all_rows:
            temp_data = {}
            temp_data.update({'assignee': str(row.assignee), 'task': str(row.task)})
            data_info.append(temp_data)

        # data_info = [
        #     {assignee: 'Scott', text: 'Write a book about Marionette'},
        #     {assignee: 'Andrew', text: 'Do some coding'}
        # ]
        context = {
            'data_info': data_info,
            'error': error
        }
        error = ""
        return self.render_response('user.html', **context)

    def post(self):
        global error
        self.assignee = self.request.form.get('assignee_name')
        self.task = self.request.form.get('task')
        self.add = self.request.form.get('add', None)
        self.update = self.request.form.get('update', None)
        self.remove = self.request.form.get('remove', None)
        if not self.add is None:
            self.user_task_detail = UserTaskDetail(key_name=self.assignee, assignee=self.assignee, task=self.task)
            self.user_task_detail.put()
            return self.redirect('user')
        elif not self.update is None:
            all_rows = UserTaskDetail.all()
            flag = 0
            for row in all_rows:
                if row.assignee == self.assignee:
                    flag = 1
                    row.task = self.task
                    row.put()
                    break
            if flag == 0:
                error = "Assignee doesn't exist"
            return self.redirect('user')
        else:
            # delete only the first occurance
            result = UserTaskDetail.all()
            for row in result:
                if row.assignee == self.assignee:
                    row.delete()
                    break

        return self.redirect('user')