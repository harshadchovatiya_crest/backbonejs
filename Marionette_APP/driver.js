// var ToDoModel = Backbone.Model.extend({
//   defaults: {
//     assignee: '',
//     text: ''
//   },

//   validate: function(attrs) {
//   	console.log("Inside Model");
//   	console.log(attrs);
//     var errors = {};
//     var hasError = false;
//     if (!attrs.assignee) {
//       errors.assignee = 'assignee must be set';
//       hasError = true;
//     }
//     if (!attrs.text) {
//       errors.text = 'text must be set';
//       hasError = true;
//     }

//     if (hasError) {
//     	console.log(errors);
//       return errors;
//     }
//   }
// });

// var ToDo = Marionette.LayoutView.extend({
//   tagName: 'li',
//   template: '#todoitem'
// });


// var TodoList = Marionette.CompositeView.extend({
//   el: '#app-hook',
//   template: '#todolist',

//   childView: ToDo,
//   childViewContainer: 'ul',

//   ui: {  // 1
//     assignee: '#id_assignee',
//     form: 'form',
//     text: '#id_text'
//   },

//   triggers: {  // 2
//     'submit @ui.form': 'AddTodoItem' //add:todo:item
//   },

//   collectionEvents: {  // 3
//     add: 'itemAdded'
//   },

//   onAddTodoItem: function() {  // 4
//     this.model.set({
//       assignee: this.ui.assignee.val(),
//       text: this.ui.text.val()
//     });

//     if (this.model.isValid()) {
//       var items = this.model.pick('assignee', 'text');
//       console.log(items)
//       this.collection.add(items);
//     }
//     else {
//     	alert("Assignee and ToDoText must not be empty");
//     }
//   },

//   itemAdded: function() {  // 6
//     this.model.set({
//       assignee: '',
//       text: ''
//     });
//   }
// });

// var todo = new TodoList({
//   collection: new Backbone.Collection([
//     {assignee: 'Scott', text: 'Write a book about Marionette'},
//     {assignee: 'Andrew', text: 'Do some coding'}
//   ]),
//   model: new ToDoModel()
// });

// todo.render();

var ToDoModel = Backbone.Model.extend({
  defaults: {
    assignee: '',
    text: ''
  },

  validate: function(attrs) {
    var errors = {};
    var hasError = false;
    if (!attrs.assignee) {
      errors.assignee = 'assignee must be set';
      hasError = true;
    }
    if (!attrs.text) {
      errors.text = 'text must be set';
      hasError = true;
    }

    if (hasError) {
      return errors;
    }
  }
});

var FormView = Marionette.LayoutView.extend({
  tagName: 'form',
  template: '#form',

  triggers: {
    submit: 'add:todo:item'
  },

  modelEvents: {
    change: 'render'
  },

  ui: {
    assignee: '#id_assignee',
    text: '#id_text'
  }
});

var ToDo = Marionette.LayoutView.extend({
  tagName: 'li',
  template: '#todoitem',
});


var ListView = Marionette.CollectionView.extend({
  tagName: 'ul',
  childView: ToDo
});

var TodoView = Marionette.LayoutView.extend({
  el: '#app-hook',

  template: '#layout',

  regions: {
    form: '.form',
    list: '.list'
  },

  collectionEvents: {
    add: 'itemAdded'
  },

  onShow: function() {
    var formView = new FormView({model: this.model});
    var listView = new ListView({collection: this.collection});

    this.showChildView('form', formView);
    this.showChildView('list', listView);
  },

  onChildviewAddTodoItem: function(child) {
    this.model.set({
      assignee: child.ui.assignee.val(),
      text: child.ui.text.val()
    });
    // , {validate: true});
	if (this.model.isValid()) {
      var items = this.model.pick('assignee', 'text');
      console.log("Hey " + items.assignee);
      //console.log(this.collection.length);
      // var flag=0;
      // for(var i=0; i<this.collection.length; i++) {
      //   if(this.collection.models[i].get('assignee') == items.assignee) {
      //     this.collection.set([this.collection.models[i],{'assignee': items.assignee, 'text': items.text}]);
      //     flag = 1;
      //     break; 
      //   }
      // }
      // if(flag==0) {
      this.collection.add(items);  
      // this.collection.models[1] = {assignee:'me', text: 'me'};
      //this.collection.set([this.collection.models[1], {assignee: 'me', text: 'working'}]);
      // }
      // console.log(this.collection.models[0].get('assignee'));
      //this.collection.set([this.collection.models[1],{assignee: 'Me', text: 'working'}]);
      // this.collection.add(items);
      //this.collection.set({'assignee': 'Working'});
    }
    else {
    	alert("Assignee and ToDoText must not be empty");
    }
  },

  itemAdded: function() {
    this.model.set({
      assignee: '',
      text: ''
    });
  }
});

var initialData = {
  items: [
    {assignee: 'Scott', text: 'Write a book about Marionette'},
    {assignee: 'Andrew', text: 'Do some coding'}
  ]
};


var App = new Marionette.Application({
  onStart: function(options) {
    var todo = new TodoView({
      collection: new Backbone.Collection(options.initialData.items),
      model: new ToDoModel()
    });
    todo.render();
    todo.triggerMethod('show');
  }
});

App.start({initialData: initialData});