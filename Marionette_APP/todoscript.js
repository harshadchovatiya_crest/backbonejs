// var ToDoModel = Backbone.Model.extend({
//   defaults: {
//     assignee: '',
//     text: ''
//   },
// });


// var ToDo = Marionette.LayoutView.extend({
//   tagName: 'tr',
//   template: '#user-list',
// });


// var ListView = Marionette.CollectionView.extend({
//   childView: ToDo
// });

// var TodoView = Marionette.LayoutView.extend({
//   el: '#user-task-list',

//   template: '#list-template',

//   regions: {
//     list: '#list'
//   },

//   onShow: function() {
//     var listView = new ListView({collection: this.collection});

//     this.showChildView('list', listView);
//   },

// });

// var initialData = {
//   items: [
//     {assignee: 'Scott', text: 'Write a book about Marionette'},
//     {assignee: 'Andrew', text: 'Do some coding'}
//   ]
// };


// var App = new Marionette.Application({
//   onStart: function(options) {
//     var todo = new TodoView({
//       collection: new Backbone.Collection(options.initialData.items),
//       model: new ToDoModel()
//     });
//     todo.render();
//     todo.triggerMethod('show');
//   }
// });

// App.start({initialData: initialData});

var data_list = new Backbone.Collection([
    {assignee: 'Scott', text: 'Write a book about Marionette'},
    {assignee: 'Andrew', text: 'Do some coding'}
  ]);

var formview = Marionette.LayoutView.extend({
  el: '#form-view',
  template: '#form-view-template',
  collection: data_list,
  events: {
    'click button': 'AddItems'
  },
  collectionEvents: {

  },
  ui: {
    assignee: '#assignee',
    task: '#task'
  },

  AddItems: function() {
    console.log('Here');
    data_list.add({assignee: this.ui.assignee.val(), text: this.ui.task.val()});
  }
});

var formv = new formview();

var helloWorld = Marionette.ItemView.extend({
  tagName: 'li',
  template: '#user-list-data'
});

var displayList = Marionette.CollectionView.extend({
  el: '#user-list',

  tagName: 'ul',
  childView: helloWorld,

  onRender: function() {
    formv.render();
  }

});

var displaylist = new displayList({
  collection: data_list
});

displaylist.render();