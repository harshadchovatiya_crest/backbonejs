var userModel = Backbone.Model.extend({
	defaults: {
		name: "pablo",
		age: 32
	}
});

var user_model = new userModel({
	name: "Mike",
	age: 32
});

var UserView = Backbone.View.extend({  
	// Set the element we attach our view to  
	el: "#userList",  
	// Set up our template  
	template: _.template($("#userTemplate").html()),  
	// Initialize our view  
	initialize: function () {  
		// Listen for changes to the model and render the view  
		this.listenTo(this.model, "change", this.render);  
		// Render the view to start  
		this.render();  
    },  
    // Render the view  
    render: function () {  
        // Append the rendered view to the view’s element  
        this.$el.html(this.template(this.model.attributes));  
        // Return this (used for chaining)  
        return this;  
	}  
});  
  
var user_view = new UserView({  
	model: user_model  
});