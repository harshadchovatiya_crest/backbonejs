var fView = Marionette.ItemView.extend({
	template: '#first-view-template'
});

var sView = Marionette.ItemView.extend({
	template: '#second-view-template'
});

var tView = Marionette.ItemView.extend({
	template: '#third-view-template'
});

var gView = Marionette.LayoutView.extend({
	el: 'body',
	regions: {
		firstview: '#first-view',
		secondview: '#second-view',
		thirdview: '#third-view'
	}
});

var gview = new gView();
gview.firstview.show(new fView());