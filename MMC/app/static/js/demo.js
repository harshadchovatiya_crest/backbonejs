var Record = Backbone.Model.extend({
    defaults: {
        device: ["GT-I9300","HTC Desire", "C1605","GT-S5360" ,"GT-I9100", "GT-S53160" ],

        session: d1
    }
});
var chart = {
	type: 'column'
};
var title = {
	text: 'Top devices by sessions'   
};
var yAxis = {
	min: 0,
	title: {
	 text: 'Sessions'
	}
};
var tooltip = {
headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
footerFormat: '</table>',
shared: true,
useHTML: true
};
var plotOptions = {
column: {
   pointPadding: 0.2,
   borderWidth: 0
}
};
var json = {};
json.chart = chart;
json.title = title;
json.tooltip = tooltip;
json.yAxis = yAxis;
json.plotOptions = plotOptions;

var graphView = Marionette.CompositeView.extend({
	el: '#div1',
	model: new Record(),
    template: '#add-data',

    itemAdded: function() {
		console.log("Working...");
		var xAxis = {
			title: {
				text: 'Devices'
			},
			categories: this.model.get('device'),
		   	crosshair: true
		};
		var series = [
		{
		   name: 'Sessions',
		   data: this.model.get('session')
		}
		];
		json.xAxis = xAxis;
		json.series = series;
		$('#container').highcharts(json);
        t.render();
	}
});

var x = new graphView();
x.render();

var tableview = Marionette.View.extend({
    el: $('.items-list'),
    template: _.template($('#display-data').html()),
    initialize: function() {},
    render: function() {
        var devices = x.model.get('device');
        var sessions = x.model.get('session');
        console.log("Inside table");
        this.$el.html('');
        for (var i = 0; i <= devices.length - 1; i++) {
            this.$el.append(this.template({ device: devices[i], session: sessions[i] }));
        }
    }
});

var t = new tableview();

$(document).ready(function() {
    x.itemAdded();
})