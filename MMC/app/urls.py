# -*- coding: utf-8 -*-
"""URL definitions."""
from tipfy.routing import Rule

rules = [
    Rule('/', name='hello-world', handler='hello_world.handlers.HelloWorldHandler'),
    Rule('/pretty', name='hello-world-pretty', handler='hello_world.handlers.PrettyHelloWorldHandler'),
    Rule('/first', name='hello-world-first', handler='hello_world.handlers.FirstHelloWorldHandler'),
    Rule('/demo', name='hello-world-demo', handler='hello_world.handlers.DemoHandler'),
    Rule('/user', name='hello-world-user', handler='hello_world.handlers.UserHandler'),
]
