// Backbone Model to store and get data

var Item = Backbone.Model.extend(
    {
        defaults: {
            item_name: "",
            category: "",
            price: ""
        }
    }
);

// Backbone Collection

var Items = Backbone.Collection.extend({});


// Instantiate two items

var item_1 = new Item({
    item_name: "Sony Tv",
    category: "Electronic",
    price: "11000"
});

var item_2 = new Item({
    item_name: "Asus Zenfone",
    category: "Electronic",
    price: "15000"
});


// Instantiate collection

var items = new Items([item_1, item_2]);

// Backbone Views for one item

var ItemView = Backbone.View.extend({
    model: new Item(),
    tagName: 'tr',
    initialize: function() {
        // console.log("here in itemview")
        this.template = _.template($('.items-list-template').html());
        console.log(this.template)
    },

    events:{
        'click .edit-item': 'edit_item',
        'click .delete-item': 'delete_item',
        'click .cancel-item': 'cancel_item',
        'click .update-item': 'update_item'
    },

    edit_item: function(){
        $('.edit-item').hide();
        $('.delete-item').hide();
        $('.update-item').show();
        $('.cancel-item').show();

        var item_name = this.$('.item-name').html();
        var category = this.$('.category').html();
        var price = this.$('.price').html();

        this.$('.item-name').html('<input type="text" class="form-control item-update" value=' + item_name + '>')
        this.$('.category').html('<input type="text" class="form-control category-update" value=' + category + '>')
        this.$('.price').html('<input type="text" class="form-control price-update" value=' + price + '>')
    },

    delete_item: function(){
        console.log("you clicked on delete");
        this.$el.remove();
    },

    cancel_item: function(){
        console.log("you clicked on cancel");
            itemsView.render();
        },

    update_item: function(){
        this.model.set('item_name', $('.item-update').val());
        this.model.set('category', $('.category-update').val());
        this.model.set('price', $('.price-update').val());
    },


    render: function(){
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

// Backbone view for all Items
var ItemsView = Backbone.View.extend({
    model: items,
    el: $('.items-list'),
    initialize: function(){
        var self = this;
        this.model.on("add", this.render, this);
        this.model.on("change", function(){
            setTimeout(function(){
                self.render();
            }, 30)
        }, this);

    },

    render: function(){
        var self = this;
        this.$el.html('');
        // console.log("Inside itemsview")
        _.each(this.model.toArray(), function(item){
            self.$el.append((new ItemView({model: item})).render().$el);
        });
        return this;
    }
});

var itemsView = new ItemsView();


$(document).ready(function() {
	itemsView.render();
	// setTimeout(function(){ alert("Hello"); }, 3000);
    $('.add-item').on('click', function(){
        var item = new Item({
            item_name: $('.item-name-input').val(),
            category: $('.category-input').val(),
            price: $('.price-input').val()
        });
        itemname = item.get('item_name');
        category = item.get('category');
        price = item.get('price');
        if (itemname == "" || category == "" || price == "") {
        	alert("Empty value not allowed...!!");
        }
        else {
	        $('.item-name-input').val('');
	        $('.category-input').val('');
	        $('.price-input').val('');
	        console.log("Item added.....")
	        console.log(item.toJSON());
	        items.add(item);        	
        }
    });
})